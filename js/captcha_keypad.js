(function ($, Drupal, once) {
    Drupal.behaviors.captchaKeypad = {
        attach: function (context, settings) {
            var clear = '<span class="clear">' + Drupal.t('Clear') + '</span><br/>' + '<span class="message"></span>';
            var elements = once('captcha-keypad', '.captcha-keypad-wrapper', context);
            elements.forEach(function (e) {
                $(e).find('.form-item-captcha-response').append(clear);
                $(e).find('.captcha-keypad-keypad-used').val('');
                $(e).find('.captcha-response').val('').keyup(function () {
                    $(this).parent().find('.captcha-response').val('');
                    $(this).parent().find('.message').css('color', 'red').html(Drupal.t('Use keypad ->'));
                });
                $(e).find('.form-item-captcha-response .clear').click(function () {
                    $(this).parent().find('.captcha-response').val('');
                    $(this).parent().find('.message').html('');
                });
                $(e).find('.captcha-keypad .inner span').click(function () {
                    $(this).parent().parent().parent().find('.captcha-keypad-keypad-used').val('1');
                    $(this).parent().parent().parent().find('.message').html('');
                    var input = $(this).parent().parent().parent().find('.captcha-response');
                    input.val(input.val() + $(this).text());
                });
            });
        }
    }
})(jQuery, Drupal, once);
