<?php

namespace Drupal\captcha_keypad\Form;

use Drupal\comment\Entity\CommentType;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * CaptchaKeypad Settings Form.
 */
class CaptchaKeypadSettingsForm extends ConfigFormBase {

  /**
   * The module manager service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $moduleConfig;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a CaptchaKeypadSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface: $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module manager service.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache tags invalidator.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typed_config_manager,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler,
    CacheTagsInvalidatorInterface $cache_tags_invalidator,
  ) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->moduleConfig = $this->config('captcha_keypad.settings');
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('cache_tags.invalidator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'captcha_keypad_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['captcha_keypad.settings'];
  }

  /**
   * Configuration form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_ids = [];

    $form['captcha_keypad_code_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Code size'),
      '#description' => $this->t('Size of the code.'),
      '#size' => 2,
      '#maxlength' => 2,
      '#default_value' => $this->moduleConfig->get('captcha_keypad_code_size') ?: 5,
      '#required' => TRUE,
    ];

    $form['captcha_keypad_shuffle_keypad'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Shuffle keypad'),
      '#description' => $this->t('Selecting this option will make the keys appear in random order.'),
      '#default_value' => $this->moduleConfig->get('captcha_keypad_shuffle_keypad'),
    ];

    if ($this->moduleHandler->moduleExists('captcha')) {
      // When Captcha module is enabled it will take care of placements.
      $url = Url::fromUri('internal:/admin/config/people/captcha/captcha-points');
      $link = new Link('Captcha settings page', $url);
      $form['captcha_keypad_captcha'] = [
        '#markup' => $this->t('Captcha module is enabled, please configure placements on @link.', ['@link' => $link->toString()]),
      ];
    }
    else {
      // Contact.
      if ($this->moduleHandler->moduleExists('contact')) {
        $ids = $this->entityTypeManager->getStorage('contact_form')->getQuery()->execute();
        if ($ids) {
          foreach ($ids as $id) {
            $form_ids['contact_message_' . $id . '_form'] = $this->t('Contact: :id', [':id' => $id]);
          }
        }
      }

      // User.
      if ($this->moduleHandler->moduleExists('user')) {
        $form_ids['user_register_form'] = $this->t('User: register');
        $form_ids['user_pass'] = $this->t('User: Forgot password');
        $form_ids['user_login_form'] = $this->t('User: Login');
        $form_ids['user_login_block'] = $this->t('User: Login block');
      }

      // Comment.
      if ($this->moduleHandler->moduleExists('comment')) {
        if ($comment_types = CommentType::loadMultiple()) {
          foreach ($comment_types as $id => $item) {
            $form_ids['comment_' . $id . '_form'] = $this->t('Comment: :item', [':item' => $item->getDescription()]);
          }
        }
      }

      // Forum.
      if ($this->moduleHandler->moduleExists('forum')) {
        $form_ids['comment_comment_forum_form'] = $this->t('Forum: comment');
      }

      $form['captcha_keypad_forms'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Forms'),
        '#options' => $form_ids,
        '#default_value' => $this->moduleConfig->get('captcha_keypad_forms') ?: [],
        '#description' => $this->t('Select which forms to add captcha keypad.'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->moduleConfig;
    $config->set('captcha_keypad_code_size', $form_state->getValue('captcha_keypad_code_size'));
    $config->set('captcha_keypad_shuffle_keypad', $form_state->getValue('captcha_keypad_shuffle_keypad'));
    if (!$this->moduleHandler->moduleExists('captcha')) {
      $config->set('captcha_keypad_forms', $form_state->getValue('captcha_keypad_forms'));
    }
    $config->save();

    parent::submitForm($form, $form_state);
    $this->cacheTagsInvalidator->invalidateTags(['rendered']);
  }

}
