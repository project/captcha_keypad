<?php

namespace Drupal\captcha_keypad\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides captcha keypad form.
 */
class CaptchaKeypad extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $moduleConfig;

  /**
   * Constructor for CaptchaKeypad.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Configuration object factory.
   */
  public function __construct(ConfigFactoryInterface $config) {
    $this->moduleConfig = $config->get('captcha_keypad.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Generates random codes.
   */
  private function getCode($digits) {
    // Little trick to allow testing.
    if ($digits == '99') {
      return 'testing';
    }

    // Limit size.
    if ($digits > 16) {
      $digits = 16;
    }

    $code = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
    $code = str_replace('0', '1', $code . '');

    return $code;
  }

  /**
   * Returns markup for keypad.
   */
  private function getKeypad($keypad = '') {
    $keys = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    if ($this->moduleConfig->get('captcha_keypad_shuffle_keypad')) {
      shuffle($keys);
    }
    for ($n = 0; $n <= 8; $n++) {
      $keypad .= '<span>' . $keys[$n] . '</span>';
    }

    return $keypad;
  }

  /**
   * Builds Captcha Keypad form.
   *
   * @param array $form
   *   The form array.
   * @param int $code
   *   The secret code.
   */
  public function getForm(array &$form = [], $code = NULL) {
    $size = $this->moduleConfig->get('captcha_keypad_code_size');

    if (!$code) {
      $code = $this->getCode($size);
    }

    $form['captcha_keypad'] = [
      '#title' => $this->t('Security'),
      '#type' => 'fieldset',
      '#attributes' => ['class' => ['captcha-keypad-wrapper', 'captcha-keypad-' . $code]],
      '#weight' => (isset($form['actions']['#weight']) ? $form['actions']['#weight'] - 1 : 99.5),
    ];

    $form['captcha_keypad']['captcha_response'] = [
      '#title' => $this->t('Code'),
      '#type' => 'textfield',
      '#attributes' => ['class' => ['captcha-response']],
      '#size' => $size,
      '#maxlength' => $size,
      '#required' => TRUE,
      '#cache' => ['max-age' => 0],
    ];

    $form['captcha_keypad']['captcha_keypad_hidden'] = [
      '#type' => 'hidden',
      '#value' => $code,
    ];

    $form['captcha_keypad']['captcha_keypad_keypad_used'] = [
      '#type' => 'hidden',
      '#attributes' => ['class' => ['captcha-keypad-keypad-used']],
    ];

    $form['captcha_keypad']['keypad'] = [
      '#prefix' => '<span class="captcha-keypad"><span class="inner">',
      '#markup' => $this->getKeypad(),
      '#suffix' => '</span></span>',
    ];

    $form['captcha_keypad']['code'] = [
      '#prefix' => '<span class="sequence">',
      '#markup' => $this->t('Click/tap this sequence: <strong>:code</strong>', [':code' => $code]),
      '#suffix' => '</span>',
    ];

    return $form;
  }

  /**
   * Form validation handler.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateForm(FormStateInterface &$form_state) {
    $captcha_keypad_hidden = $captcha_keypad_keypad_used = '';
    $input = $form_state->getUserInput();

    foreach (['hidden', 'keypad_used'] as $i) {
      $v = "captcha_keypad_$i";
      $$v = isset($input[$v]) ? $input[$v] : '';
    }
    $captcha_response = $input['captcha_response'];

    // For Functional tests to work.
    if ($this->moduleConfig->get('captcha_keypad_code_size') === 99 && $captcha_response === 'testing') {
      $captcha_keypad_keypad_used = '1';
      $captcha_keypad_hidden = $captcha_response;
    }

    if ($captcha_response !== $captcha_keypad_hidden || $captcha_keypad_keypad_used !== '1') {
      $form_state->setErrorByName('captcha_response', $this->t('Invalid security code.'));
    }
  }

}
