<?php

namespace Drupal\Tests\captcha_keypad\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests Captcha Keypad on User forms.
 *
 * @group   captcha_keypad
 *
 * Class CaptchaKeypadTestUser
 * @package DrupalTestsCaptchaKeypadFunctional
 */
class CaptchaKeypadTestUser extends BrowserTestBase {

    use StringTranslationTrait;

    /**
     * {@inheritdoc}
     */
    protected $defaultTheme = 'stark';

    /**
     * {@inheritdoc}
     */
    protected $profile = 'minimal';

    /**
     * Modules to enable.
     *
     * @var array
     */
    protected static $modules = ['contact', 'user', 'captcha_keypad'];

    /**
     * A user with the 'Administer Captcha keypad' permission.
     *
     * @var \Drupal\user\UserInterface
     */
    protected $adminUser;

    /**
     * Authenticated user.
     *
     * @var \Drupal\user\UserInterface
     */
    protected $user;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void {
        parent::setUp();

        // Create admin user.
        $this->adminUser = $this->drupalCreateUser(
            ['administer captcha keypad'],
            'adming',
            TRUE
        );
        $this->user = $this->drupalCreateUser();
    }

    /**
     * Test admin UI.
     */
    public function testAdminUi()
    {
        $this->drupalLogin($this->adminUser);
        $this->drupalGet('admin/config/system/captcha_keypad');
        $this->assertSession()->pageTextNotContains('Captcha module is enabled, please configure placements on Captcha settings page.');
    }

    /**
     * Test link to config.
     */
    public function testLinkToConfig()
    {
        $this->drupalLogin($this->adminUser);
        $this->drupalGet('admin/modules');
        $link = $this->xpath('//a[contains(@href, :href) and contains(@id, :id)]', [
            ':href' => '/admin/config/system/captcha_keypad',
            ':id' => 'edit-modules-captcha-keypad-links-configure',
        ]);
        $this->assertTrue(count($link) === 1, $this->t('Link to config is present'));
    }

    /**
     * Test user forms.
     */
    public function testUserForms()
    {
        $this->drupalLogin($this->adminUser);

        $this->drupalGet('admin/config/system/captcha_keypad');
        $this->submitForm(
            [
                'captcha_keypad_code_size' => 99,
                'captcha_keypad_shuffle_keypad' => FALSE,
                'captcha_keypad_forms[user_register_form]' => 1,
                'captcha_keypad_forms[user_login_form]' => 1,
                'captcha_keypad_forms[user_pass]' => 1,
            ], $this->t('Save configuration')
        );

        $this->drupalGet('admin/config/system/captcha_keypad');
        $element = $this->xpath('//input[@type="checkbox" and @name="captcha_keypad_shuffle_keypad" and @checked="checked"]');
        $this->assertTrue(count($element) === 0, $this->t('Shuffle form is not checked.'));

        $element = $this->xpath('//input[@type="text" and @id="edit-captcha-keypad-code-size" and @value="99"]');
        $this->assertTrue(count($element) === 1, $this->t('The code size is correct.'));

        $element = $this->xpath('//input[@type="checkbox" and @name="captcha_keypad_forms[user_register_form]" and @checked="checked"]');
        $this->assertTrue(count($element) === 1, $this->t('Register form is checked.'));

        $element = $this->xpath('//input[@type="checkbox" and @name="captcha_keypad_forms[user_login_form]" and @checked="checked"]');
        $this->assertTrue(count($element) === 1, $this->t('User login form is checked.'));

        $element = $this->xpath('//input[@type="checkbox" and @name="captcha_keypad_forms[user_pass]" and @checked="checked"]');
        $this->assertTrue(count($element) === 1, $this->t('Forgot password form is checked.'));

        $this->drupalLogout();

        // User password form.
        $this->drupalGet('/user/password');
        $element = $this->xpath('//input[@type="text" and @id="edit-captcha-response" and @value=""]');
        $this->assertTrue(count($element) === 1, $this->t('The input text is present.'));

        for ($i = 1; $i <= 9; $i++) {
            $element = $this->xpath('//span[@class="captcha-keypad"]/span/span[text()="' . $i . '"]');
            $this->assertTrue(count($element) === 1, $this->t('Button ' . $i . ' is present.'));
        }

        $this->assertSession()->pageTextContains($this->t('Click/tap this sequence: testing'));

        // User register form.
        $this->drupalGet('/user/register');
        $element = $this->xpath('//input[@type="text" and @id="edit-captcha-response" and @value=""]');
        $this->assertTrue(count($element) === 1, $this->t('The input text is present.'));

        // User login form.
        $this->drupalGet('user/login');
        $this->submitForm(
            [
                'name' => $this->adminUser->getAccountName(),
                'pass' => $this->adminUser->getPassword(),
            ],
            $this->t('Log in')
        );

        $element = $this->xpath('//input[@type="text" and @id="edit-captcha-response" and @value=""]');
        $this->assertTrue(count($element) === 1, $this->t('The input text is present.'));
    }

    /**
     * Test Form validation.
     */
    public function testFormValidation()
    {
        // Test submission with empty code.
        $this->drupalGet('user/login');
        $this->submitForm(
            [
                'name' => $this->user->getAccountName(),
                'pass' => $this->user->pass_raw
            ], $this->t('Log in')
        );

        $this->assertSession()->responseNotContains($this->t('Invalid security code.'));
        $this->assertSession()->responseContains($this->t('Member for'));
        $this->drupalLogout();

        // Turn on captcha keypad on login form.
        \Drupal::configFactory()
            ->getEditable('captcha_keypad.settings')
            ->set('captcha_keypad_forms', ['user_login_form' => 'user_login_form'])
            ->save();

        $this->drupalGet('user/login');
        $this->submitForm([
            'name' => $this->user->getAccountName(),
            'pass' => $this->user->pass_raw
        ], $this->t('Log in'));
        $this->assertSession()->responseContains('Code field is required.');

        // Test submission without tap or mouse click.
        $this->assertSession()->hiddenFieldExists('captcha_keypad_keypad_used');
        $this->submitForm(
            [
                'name' => $this->user->getAccountName(),
                'pass' => $this->user->pass_raw,
                'captcha_response' => '1234',
            ], $this->t('Log in')
        );
        $this->assertSession()->responseContains('Invalid security code.');

        // Enable testing mode.
        \Drupal::configFactory()
            ->getEditable('captcha_keypad.settings')
            ->set('captcha_keypad_code_size', 99)
            ->save();

        // Test submission with invalid code.
        $this->assertSession()->hiddenFieldExists('captcha_keypad_keypad_used');
        $this->submitForm(
            [
                'name' => $this->user->getAccountName(),
                'pass' => $this->user->pass_raw,
                'captcha_response' => 'testing',
            ], $this->t('Log in')
        );
        $this->assertSession()->responseContains('Member for');
    }

}
