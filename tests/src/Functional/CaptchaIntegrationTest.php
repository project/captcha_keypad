<?php

namespace Drupal\Tests\captcha_keypad\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\captcha\Functional\CaptchaWebTestBase;

/**
 * Tests Captcha Keypad integration with Captcha Module.
 *
 * @group   captcha_keypad
 *
 * Class CaptchaIntegrationTest
 * @package DrupalTestsCaptchaKeypadFunctional
 */
class CaptchaIntegrationTest extends CaptchaWebTestBase
{

    use StringTranslationTrait;

    /**
     * A user with the 'Administer Captcha keypad' permission.
     *
     * @var \Drupal\user\UserInterface
     */
    protected $adminUser;

    /**
     * Modules to enable.
     *
     * @var array
     */
    protected static $modules = ['captcha', 'captcha_keypad'];

    /**
     * {@inheritdoc}
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->adminUser = $this->drupalCreateUser(['administer captcha keypad'], 'adming', TRUE);
    }

    /**
     * Test admin UI.
     */
    public function testAdminUi()
    {
        $this->drupalLogin($this->adminUser);
        $this->drupalGet('admin/config/system/captcha_keypad');
        $this->assertSession()->pageTextContains('Captcha module is enabled, please configure placements on Captcha settings page.');
    }

    /**
     * Testing the protection of the user log in form.
     */
    public function testCaptchaLoginForm()
    {
        // Set a CAPTCHA on login form.
        /* @var \Drupal\captcha\Entity\CaptchaPoint $captcha_point */
        $captcha_point = \Drupal::entityTypeManager()
            ->getStorage('captcha_point')
            ->load('user_login_form');
        $captcha_point->setCaptchaType('captcha_keypad/Keypad');
        $captcha_point->enable()->save();

        // Check if there is a CAPTCHA on the login form (look for the title).
        $this->drupalGet('user');
        $this->assertCaptchaPresence(TRUE);

        // Try to log in, which should fail.
        $edit = [
            'name' => $this->normalUser->getDisplayName(),
            'pass' => $this->normalUser->pass_raw,
            'captcha_response' => '?',
        ];
        $this->submitForm($edit, $this->t('Log in'), self::LOGIN_HTML_FORM_ID);
        $this->assertSession()
            ->pageTextContains(self::CAPTCHA_WRONG_RESPONSE_ERROR_MESSAGE);

        // And make sure that user is not logged in:
        // check for name and password fields on ?q=user.
        $this->drupalGet('user');
        $this->assertSession()->fieldExists('name');
        $this->assertSession()->fieldExists('pass');

        // Test submission without tap or mouse click.
        $this->assertSession()->hiddenFieldExists('captcha_keypad_keypad_used');
        $this->submitForm(
            [
                'name' => $this->normalUser->getAccountName(),
                'pass' => $this->normalUser->pass_raw,
                'captcha_response' => '1234',
            ], $this->t('Log in')
        );
        $this->assertSession()
            ->pageTextContains(self::CAPTCHA_WRONG_RESPONSE_ERROR_MESSAGE);

        // Enable testing mode.
        \Drupal::configFactory()
            ->getEditable('captcha_keypad.settings')
            ->set('captcha_keypad_code_size', 99)
            ->save();

        // Go to user page.
        $this->drupalGet('user');
        $this->assertSession()->hiddenFieldExists('captcha_keypad_keypad_used');
        $this->assertSession()->hiddenFieldExists('captcha_keypad_hidden');

        // Test submission with valid code.
        $this->submitForm([
            'name' => $this->normalUser->getAccountName(),
            'pass' => $this->normalUser->pass_raw,
            'captcha_response' => 'testing',
        ], $this->t('Log in'));
        $this->assertSession()->responseContains('Member for');
    }

    /**
     * Testing if comment posting works as it should.
     */
    public function testCommentPosting() {
        // Make sure comments on pages can be saved directly without preview.
        $this->container
            ->get('state')
            ->set('comment_preview_page', DRUPAL_OPTIONAL);

        // Set a CAPTCHA on comments form.
        \Drupal::configFactory()
            ->getEditable('captcha.captcha_point.comment_comment_form')
            ->set('formId', 'comment_comment_form')
            ->set('captchaType', 'captcha_keypad/Keypad')
            ->set('label', 'comment_comment_form')
            ->save();

        // Create a node with comments enabled.
        $node = $this->drupalCreateNode();

        // Post comments on node.
        $this->drupalLogin($this->normalUser);
        $this->drupalGet('comment/reply/node/' . $node->id() . '/comment');
        $edit = $this->getCommentFormValues();
        $comment_subject = $edit['subject[0][value]'];
        $comment_body = $edit['comment_body[0][value]'];
        $edit['captcha_response'] = '0000';
        $this->submitForm($edit, $this->t('Save'), 'comment-form');
        $this->assertSession()->pageTextContains(self::CAPTCHA_WRONG_RESPONSE_ERROR_MESSAGE);

        // Enable testing mode.
        \Drupal::configFactory()
            ->getEditable('captcha_keypad.settings')
            ->set('captcha_keypad_code_size', 99)
            ->save();

        // Post again.
        $this->drupalGet('comment/reply/node/' . $node->id() . '/comment');
        $edit = $this->getCommentFormValues();
        $comment_subject = $edit['subject[0][value]'];
        $comment_body = $edit['comment_body[0][value]'];
        $edit['captcha_response'] = 'testing';
        $this->submitForm($edit, $this->t('Save'), 'comment-form');
        $this->assertSession()->pageTextContains('Your comment has been posted');
        $this->assertSession()->pageTextContains($comment_subject);
        $this->assertSession()->pageTextContains($comment_body);
        $this->assertSession()->pageTextNotContains(self::CAPTCHA_WRONG_RESPONSE_ERROR_MESSAGE);
    }

}
