<?php

namespace Drupal\Tests\captcha_keypad\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests Captcha Keypad on comment forms.
 *
 * @group   captcha_keypad
 *
 * Class CaptchaKeypadTest
 * @package DrupalTestsCaptchaKeypadFunctional
 */
class CaptchaKeypadTestComment extends BrowserTestBase
{

    /**
     * {@inheritdoc}
     */
    protected $defaultTheme = 'stark';

    /**
     * {@inheritdoc}
     */
    protected $profile = 'minimal';

    /**
     * Modules to enable.
     *
     * @var array
     */
    protected static $modules = ['node', 'field_ui', 'comment', 'captcha_keypad'];

    /**
     * A user with the 'Administer Captcha keypad' permission.
     *
     * @var \Drupal\user\UserInterface
     */
    protected $adminUser;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        // Create admin user.
        $this->adminUser = $this->drupalCreateUser(['administer captcha keypad'], 'adming', TRUE);
    }

    /**
     * Test for Comment forms.
     */
    public function testCaptchaKeypadCommentForm()
    {
        $this->drupalLogin($this->adminUser);

        // Create article node type.
        $this->drupalCreateContentType(
            [
                'type' => 'article',
                'name' => 'Article',
            ]
        );

        // Create comment and attach to content type.

        // Enable Captcha keypad on comment form.
        $this->drupalGet('admin/config/system/captcha_keypad');

        // Create content.
        // @todo add tests

        // Test Captcha Keypad.
    }

}
