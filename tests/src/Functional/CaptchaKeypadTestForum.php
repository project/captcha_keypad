<?php

namespace Drupal\Tests\captcha_keypad\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests Captcha Keypad on contact pages.
 *
 * @group   captcha_keypad
 *
 * Class CaptchaKeypadTestForum
 * @package DrupalTestsCaptchaKeypadFunctional
 */
class CaptchaKeypadTestForum extends BrowserTestBase {

    use StringTranslationTrait;

    /**
     * {@inheritdoc}
     */
    protected $defaultTheme = 'stark';

    /**
     * {@inheritdoc}
     */
    protected $profile = 'minimal';

    /**
     * Modules to enable.
     *
     * @var array
     */
    protected static $modules = ['forum', 'captcha_keypad'];

    /**
     * A user with the 'Administer Captcha keypad' permission.
     *
     * @var \Drupal\user\UserInterface
     */
    protected $adminUser;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void {
        parent::setUp();

        // Create admin user.
        $this->adminUser = $this->drupalCreateUser(['administer captcha keypad'], 'adming', TRUE);
    }

    /**
     * Test for Forum forms.
     */
    public function testCaptchaKeypadForumForm() {
        $this->drupalLogin($this->adminUser);

        // Turn on Captcha keypad for the forum form.
        $this->drupalGet('admin/config/system/captcha_keypad');
        $this->submitForm([
            'captcha_keypad_code_size' => 99,
            'captcha_keypad_forms[comment_comment_forum_form]' => 1,
        ], $this->t('Save configuration'));

        $this->drupalGet('admin/config/system/captcha_keypad');
        $element = $this->xpath('//input[@type="text" and @id="edit-captcha-keypad-code-size" and @value="99"]');
        $this->assertTrue(count($element) === 1, 'The code size is correct.');

        $element = $this->xpath('//input[@type="checkbox" and @name="captcha_keypad_forms[comment_comment_forum_form]" and @checked="checked"]');
        $this->assertTrue(count($element) === 1, 'Forum form is checked.');

        $this->drupalGet('forum');

        // Create new forum topic.
        $this->drupalGet('forum/1');
        $this->clickLink($this->t('Add new Forum topic'));
        $this->submitForm([
            'title[0][value]' => 'Foo',
        ], $this->t('Save'));
        $this->assertSession()->pageTextContains('Forum topic Foo has been created.');

        // Submit form without captcha code.
        $this->submitForm([
            'comment_body[0][value]' => 'Foo',
        ], $this->t('Save'));
        $this->assertSession()->responseContains($this->t('Code field is required.'));

        // Submit the wrong code.
        $this->assertSession()->hiddenFieldExists('captcha_keypad_keypad_used');
        $this->submitForm([
            'comment_body[0][value]' => 'Foo',
            'captcha_response' => '1234',
        ], $this->t('Save'));
        $this->assertSession()->responseContains($this->t('Invalid security code.'));

        // Submit form with captcha code.
        $this->submitForm([
            'comment_body[0][value]' => 'Foo',
            'captcha_response' => 'testing',
        ], $this->t('Save'));
        $this->assertSession()->responseContains('Your comment has been posted.');
    }

}
