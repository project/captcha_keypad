<?php

namespace Drupal\Tests\captcha_keypad\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests Captcha Keypad on contact pages.
 *
 * @group   captcha_keypad
 *
 * Class CaptchaKeypadTestContact
 * @package DrupalTestscaptcha_KeypadFunctional
 */
class CaptchaKeypadTestContact extends BrowserTestBase
{

    use StringTranslationTrait;

    /**
     * {@inheritdoc}
     */
    protected $defaultTheme = 'stark';

    /**
     * {@inheritdoc}
     */
    protected $profile = 'minimal';

    /**
     * Modules to enable.
     *
     * @var array
     */
    protected static $modules = ['contact', 'captcha_keypad'];

    /**
     * A user with the 'Administer Captcha keypad' permission.
     *
     * @var \Drupal\user\UserInterface
     */
    protected $adminUser;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        // Create admin user.
        $this->adminUser = $this->drupalCreateUser(
            ['administer captcha keypad'], 'adming', true
        );
    }

    /**
     * Test for Contact forms.
     */
    public function testCaptchaKeypadContactForm()
    {
        $this->drupalLogin($this->adminUser);

        // Add new contact form.
        $this->drupalGet('admin/structure/contact/add');
        $this->submitForm(
            [
                'id' => $this->t('feedback'),
                'label' => $this->t('feedback'),
                'recipients' => 'email@example.com',
            ], $this->t('Save')
        );

        // Turn on Captcha keypad for the contact form.
        $this->drupalGet('admin/config/system/captcha_keypad');
        $this->submitForm(
            [
                'captcha_keypad_code_size' => 99,
                'captcha_keypad_forms[contact_message_personal_form]' => 1,
                'captcha_keypad_forms[contact_message_feedback_form]' => 1,
            ], $this->t('Save configuration')
        );

        $this->drupalGet('admin/config/system/captcha_keypad');
        $element = $this->xpath('//input[@type="text" and @id="edit-captcha-keypad-code-size" and @value="99"]');
        $this->assertTrue(count($element) === 1, 'The code size is correct.');

        $element = $this->xpath('//input[@type="checkbox" and @name="captcha_keypad_forms[contact_message_personal_form]" and @checked="checked"]');
        $this->assertTrue(count($element) === 1, 'Contact form is checked.');

        $element = $this->xpath('//input[@type="checkbox" and @name="captcha_keypad_forms[contact_message_feedback_form]" and @checked="checked"]');
        $this->assertTrue(count($element) === 1, 'Feedback form is checked.');

        // Submit form without captcha code.
        $this->drupalGet('contact/feedback');
        $this->submitForm(
            [
                'subject[0][value]' => 'Foo',
                'message[0][value]' => 'Bar',
            ], $this->t('Send message')
        );
        $this->assertSession()->responseContains($this->t('Code field is required.'));

        // Submit the wrong code.
        $this->drupalGet('contact/feedback');
        $this->assertSession()->hiddenFieldExists('captcha_keypad_keypad_used');
        $this->submitForm(
            [
                'subject[0][value]' => 'Foo',
                'message[0][value]' => 'Bar',
                'captcha_response' => '1234',
            ], $this->t('Send message')
        );
        $this->assertSession()->responseContains($this->t('Invalid security code.'));

        // Submit the right code.
        $this->drupalGet('contact/feedback');
        $this->assertSession()->hiddenFieldExists('captcha_keypad_keypad_used');
        $this->submitForm(
            [
                'subject[0][value]' => 'Foo',
                'message[0][value]' => 'Bar',
                'captcha_response' => 'testing',
            ], $this->t('Send message')
        );
        $this->assertSession()->responseNotContains($this->t('Invalid security code.'));
    }
}
